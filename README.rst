===========================
HTTP Mpv Youtube IPC Server
===========================

* Based on mpv, golang, and https://github.com/blang/mpv
* Cross compile, linux arm (raspbian) relsease mode and verbose ie: 
  GOOS=linux GOARCH=arm go build -ldflags "-s -w" -x

----
TODO
----

* Enable CORS in all handlers
* Run mpv internally or from tmux, ie: 
  mpv --no-video --input-ipc-server=/tmp/mpvsocket --idle
* Static responsive index.html to interact with API, it will handle youtube id/list/url
* pafy youtube key: AIzaSyCIM4EzNqi1in22f4Z3Ru3iYvLaY8tc3bo (taken from: https://github.com/mps-youtube/pafy/blob/develop/pafy/g.py)
* Playlist load (no cors enabled):
  https://www.youtube.com/list_ajax?style=json&action_get_list=1&list=RDEMdWFlwEwJNijvbqWc96cuXw
* Analyze materialize + mithril, or bourbon + plain js
* Analize PWA
* observer media-title property to websocket
* Possible theme http://www.colourlovers.com/palette/3189774/Another_Whack
