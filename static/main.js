;(function () {
  var _l = console.log.bind(console)
  var _c = document.createElement.bind(document)
  var lastTitle = ''

  var form = document.querySelector('form')
  var url = form.querySelector('[name=url]')
  url.focus()

  form.addEventListener('submit', function (ev) {
    ev.preventDefault()
    var urlt = url.value.trim()
    if (urlt == '') {
      return
    }
    var param = getParam(urlt)
    fetch('/yt/' + param.v)
    if (param.list) {
      getMusicInfo(param.v)
    }
  })

  form.querySelectorAll('.cmd').forEach(function (cmd) {
    cmd.addEventListener('click', function (ev) {
      var name = ev.target.name
      ev.preventDefault()
      fetch('/cmd/' + name)
    })
  })

  document.querySelectorAll('#examples li a').forEach(function (anchor) {
    anchor.addEventListener('click', function (ev) {
      ev.preventDefault()
      var link = ev.target.textContent
      url.value = link
    })
  })

  getMusicInfo('')
  var port = location.port
  if (port != '') port = ':' + port
  var ws = new WebSocket('ws://' + location.host + port + '/wsocket')
  window.ws = ws
  ws.addEventListener('message', function (ev) {
    // _l(ev)
    if (ev.data) {
      try {
        var data = JSON.parse(ev.data)
      } catch(e) {
        return _e('cant parse', ev.data, e)
      }
      var curr = ''
      var changed = false
      for (var i = 0; i < data.length; i++) {
        if (data[i].current) {
          curr = data[i].current
          if (data[i].current != data[i]['media-title']) {
            changed = true
          }
        }
      }
      if (curr && changed) {
        setMusicInfoSel(data[0].current)
      }
    }
  })

  function getParam(url) {
    url = url.replace(/.*\//, '')
    url = url.replace(/.*watch\?/, '')
    var params = url.split('&')
    var v = ''
    var list = false
    for (var i = 0; i < params.length; i++) {
      params[i] = params[i].split('=')
      if (params[i].length > 1) { 
        if (params[i][0] == 'list' || params[i][0] == 'l') {
          v = params[i][1]
          list = true
        }
        if (v == '' && (params[i][0] == 'value' || params[i][0] == 'v')) {
          v = params[i][1]
          list = false
        }
      }
    }
    if (v == '' && params.length == 1 && params[0].length == 1) {
      v = params[0][0]
      list = params[0][0].length > 11
    }
    return {v: v, list: list}
  }

  function get(url, cb) {
    var req = new XMLHttpRequest()
    req.addEventListener('load', cb)
    req.open('GET', url)
    req.send()
    return req
  }

  function getMusicInfo(listId) {
    // @TODO clear playlist
    var cntPlaylist = document.getElementById('cnt-playlist')
    cntPlaylist.innerHTML = '…'
    fetch('/ytList/' + listId)
    .then(res => res.json())
    .then(data => {
      cntPlaylist.innerHTML = ''
      var ul = _c('ul')
      cntPlaylist.appendChild(ul)

      data.video.forEach(function (vid) {
        var a = _c('a')
        var img = _c('img')
        var li = _c('li')
        var id = vid.encrypted_id
        var rid = 'RD' + id

        img.src = vid.thumbnail
        a.href = 'https://youtu.be/' + id
        a.title = vid.title + ' (' + vid.duration + ')'
        li.id = 'vid-' + id
        if (vid.encrypted_id == data.current) {
          setTitle(a.title)
          li.className = 'selected'
        }
        a.textContent = vid.title + ' (' + vid.duration + ')'
        a.appendChild(_c('br'))
        a.appendChild(img)
        a.addEventListener('click', function (ev) {
          ev.preventDefault()
          url.value = rid
        })
        li.appendChild(a)
        ul.appendChild(li)
      })
    })
  }
  function setMusicInfoSel(id) {
    var videos = document.querySelectorAll('#cnt-playlist li')
    videos.forEach(function (video) {
      if (video.id == 'vid-' + id) {
        video.className = 'selected'
        setTitle(video.querySelector('a').title)
      } else {
        video.className = ''
      }
    })
  }
  function setTitle(title) {
    if (!title) {
      return
    }
    var cntTitle = document.getElementById('media-title')
    cntTitle.textContent = title
  }
})()
