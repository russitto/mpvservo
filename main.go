package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net"
	"net/http"
	"os"
	"os/exec"
	"strings"
	"time"

	"github.com/blang/mpv"
	"github.com/gorilla/websocket"
)

var upgrader = websocket.Upgrader{} // use default options
var uc net.Conn
var mpvc mpv.Client

func main() {
	listen := ":8080"
	lastYtId := ""

	socketFile := "/tmp/mpvsocket"
	if len(os.Args) > 1 {
		socketFile = os.Args[1]
	}
	if len(os.Args) > 2 {
		listen = os.Args[2]
	}

	_, err := runMpv(socketFile)
	if err != nil {
		log.Fatal(err)
	}
	time.Sleep(2 * time.Second)

	uc, err = net.Dial("unix", "/tmp/mpvsocket")
	if err != nil {
		log.Fatal(err)
	}
	defer func() {
		err := uc.Close()
		if err != nil {
			log.Fatal(err)
		}
	}()

	mpvll := mpv.NewIPCClient(socketFile)
	mpvc = mpv.Client{mpvll}

	_, err = mpvc.Exec("observe_property", 1, "media-title")
	if err != nil {
		log.Fatal(err)
	}

	// Build in low level json api
	http.Handle("/lowlevel", mpv.HTTPServerHandler(mpvll))
	http.HandleFunc("/", http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		http.Redirect(w, r, "/s/", 301)
	}))

	// Your own api based on mpv.Client
	http.HandleFunc("/file/", http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		log.Printf("Request URL: %s", r.RequestURI)
		path := strings.Replace(r.RequestURI, "/file/", "", 1)
		fmt.Fprintln(w, path)
		log.Println(mpvc.Loadfile(path, mpv.LoadFileModeAppendPlay))
	}))
	/*
		http.HandleFunc("/playlist/", http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			log.Printf("Request URL: %s", r.RequestURI)
			path := strings.Replace(r.RequestURI, "/playlist/", "", 1)
			fmt.Fprintln(w, path)
			log.Println(mpvc.LoadList(path, mpv.LoadListModeReplace))
		}))
	*/
	http.HandleFunc("/yt/", http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		/*
			w.Header().Set("Access-Control-Allow-Origin", "*")
			w.Header().Set("Access-Control-Allow-Methods", "POST, GET, OPTIONS, PUT, DELETE")
			w.Header().Set("Access-Control-Allow-Headers",
				"Accept, Content-Type, Content-Length, Accept-Encoding, X-CSRF-Token, Authorization")
		*/
		log.Printf("Request URL: %s", r.RequestURI)
		path := strings.Replace(r.RequestURI, "/yt/", "", 1)
		lastYtId = path
		fmt.Fprintln(w, path)
		log.Println(mpvc.Loadfile(fmt.Sprintf("ytdl://%s", path), mpv.LoadFileModeReplace))
	}))
	http.HandleFunc("/ytInfo/", http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		log.Printf("Request URL: %s", r.RequestURI)

		path := strings.Replace(r.RequestURI, "/ytInfo/", "", 1)
		if path == "" {
			path = lastYtId
		}
		url := fmt.Sprintf("http://www.youtube.com/oembed?url=http://www.youtube.com/watch?v=%s&format=json", path)
		log.Println(url)
		resp, err := http.Get(url)

		if err != nil {
			fmt.Fprintf(w, "Error: %s", err)
			return
		}
		w.Header().Set("Content-Type", "application/json")
		body, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			fmt.Fprintf(w, "Error: %s", err)
			return
		}
		fmt.Fprint(w, string(body))
	}))
	http.HandleFunc("/ytList/", http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		log.Printf("Request URL: %s", r.RequestURI)

		path := strings.Replace(r.RequestURI, "/ytList/", "", 1)
		if path == "" {
			path = lastYtId
		}
		url := fmt.Sprintf("https://www.youtube.com/list_ajax?style=json&action_get_list=1&list=%s", path)
		resp, err := http.Get(url)

		if err != nil {
			fmt.Fprintf(w, "Error: %s", err)
			return
		}
		w.Header().Set("Content-Type", "application/json")
		body, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			fmt.Fprintf(w, "Error: %s", err)
			return
		}
		current, err := mpvc.GetProperty("filename")
		if err != nil {
			fmt.Fprintf(w, "Error: %s", err)
			return
		}
		sbody := fmt.Sprintf("{\"current\":%s,%s", current, string(body[1:]))
		fmt.Fprint(w, sbody)
	}))
	http.HandleFunc("/cmd/seekRight", http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		fmt.Fprintln(w, mpvc.Seek(10, mpv.SeekModeRelative))
	}))
	http.HandleFunc("/cmd/seekLeft", http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		fmt.Fprintln(w, mpvc.Seek(-10, mpv.SeekModeRelative))
	}))
	http.HandleFunc("/cmd/volUp", http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		err := setVolume(mpvc, 10.0)
		if err != nil {
			fmt.Fprintf(w, "Error: %s", err)
			return
		}
		fmt.Fprintln(w, "ok up")
	}))
	http.HandleFunc("/cmd/volDown", http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		err := setVolume(mpvc, -10.0)
		if err != nil {
			fmt.Fprintf(w, "Error: %s", err)
			return
		}
		fmt.Fprintln(w, "ok down")
	}))
	http.HandleFunc("/cmd/play", http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		fmt.Fprintln(w, mpvc.SetPause(false))
	}))
	http.HandleFunc("/cmd/pause", http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		fmt.Fprintln(w, mpvc.SetPause(true))
	}))
	http.HandleFunc("/cmd/prev", http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		fmt.Fprintln(w, mpvc.PlaylistPrevious())
	}))
	http.HandleFunc("/cmd/next", http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		fmt.Fprintln(w, mpvc.PlaylistNext())
	}))
	http.HandleFunc("/value/", http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		name := strings.Replace(r.RequestURI, "/value/", "", 1)
		value, err := mpvc.GetProperty(name)
		if err != nil {
			fmt.Fprintf(w, "Error: %s", err)
			return
		}
		fmt.Fprintf(w, "%s", value)
	}))
	http.HandleFunc("/playlist", http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		res, err := mpvc.Exec("get_property", "playlist")
		if err != nil {
			fmt.Fprintf(w, "Error: %s", err)
			return
		}
		out, err := json.Marshal(res.Data)
		if err != nil {
			fmt.Fprintf(w, "Error: %s", err)
			return
		}
		w.Header().Set("Content-Type", "application/json")
		fmt.Fprintf(w, "%s", out)
	}))
	/*
		http.HandleFunc("/cmd/fullscreen", http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			err := mpvc.SetProperty("fullscreen", true)
			if err != nil {
				fmt.Fprintf(w, "Error: %s", err)
				return
			}
			fmt.Fprintf(w, "ok")
		}))
	*/

	http.HandleFunc("/wsocket", wsocket)

	fs := http.FileServer(http.Dir("./static"))
	http.Handle("/s/", http.StripPrefix("/s", fs))
	log.Println("Socket:", socketFile, " - Listening on:", listen)
	log.Fatal(http.ListenAndServe(listen, nil))
}

func setVolume(mpvc mpv.Client, plus float64) error {
	vol, err := mpvc.Volume()
	if err != nil {
		return err
	}
	vol += plus
	if vol >= 100.0 {
		vol = 100.0
	}
	if vol < 0.0 {
		vol = 0
	}
	return mpvc.SetProperty("volume", vol)
}

func runMpv(socketFile string) (*exec.Cmd, error) {
	cmd := exec.Command("mpv", "--no-video", "--idle", fmt.Sprintf("--input-ipc-server=%s", socketFile))
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr
	err := cmd.Start()
	if err != nil {
		log.Fatal(err)
	}
	return cmd, err
}

func wsocket(w http.ResponseWriter, r *http.Request) {
	wc, err := upgrader.Upgrade(w, r, nil)
	if err != nil {
		log.Print("upgrade:", err)
		return
	}
	defer func() {
		err := wc.Close()
		if err != nil {
			log.Print("upgrade:", err)
		}
	}()

	data := make([]byte, 1024)
	for {
		lng, err := uc.Read(data)
		if err != nil {
			log.Println("unix socket:", err)
			break
		}
		current, err := mpvc.GetProperty("filename")
		if err != nil {
			log.Println("unix socket:", err)
			break
		}
		mediaTitle, err := mpvc.GetProperty("media-title")
		if err != nil {
			log.Println("unix socket:", err)
			break
		}
		sdata := strings.Replace(string(data[0:lng-1]), "\n", ",", -1)
		smessage := fmt.Sprintf("[{\"current\": %s, \"media-title\": %s }, %s]", current, mediaTitle, sdata)
		err = wc.WriteMessage(1, []byte(smessage)) // 1 => TextMessage type
		if err != nil {
			log.Println("unix socket:", err)
			break
		}
	}
}
